## Introduction
In this guide, we will setup a mail server that can be used both internally and externally. For this we will be using:
* Debian/Ubuntu Server
* Postfix
* Dovecot

To test we will use the following:
* mailutils
* telnet

This guide have been tested on a Ubuntu server and is written to be used on one. But any Debian distro should work the same.

I don't take responsibility of how secure the configuration is. Use at your own risk.
## Debug
As you create and test the mail server, errors is bound to happen. Everything that goes wrong in with mail services will be logged in the file `/var/log/mail.log`. 
To make the troubleshooting easier, we will open the log file and listen to any changes:

```
tail -f /var/log/mail.log
```
## Setup Postfix
Update the machine and install postfix and mailutils. Postfix is the SMTP service on the server. Mailutils is used to check and send mail from the command line.
```
sudo apt-get update
sudo apt install postfix mailutils
```
* Internet Site
* locahost.localdomain

## Setup Virtual Mailbox
Postfix will save the mails as files on the server, so it need a user that owns the mails.
```
sudo groupadd -g 5000 vmail
sudo useradd -m -u 5000 -g 5000 -s /bin/bash vmail
```
----
Setting up the virtual mailboxes. We will save the mails to the users in: `/var/mail/vhosts`.
```
sudo postconf -e 'virtual_mailbox_domains = /etc/postfix/vhosts'
sudo postconf -e 'virtual_mailbox_base = /var/mail/vhosts'
sudo postconf -e 'virtual_mailbox_maps = hash:/etc/postfix/vmailbox'
sudo postconf -e 'virtual_minimum_uid = 1000'
sudo postconf -e 'virtual_uid_maps = static:5000'
sudo postconf -e 'virtual_gid_maps = static:5000'
```
----
In this file, is where the domain that the server will accept. This can be anything, as long as it follows the domain name syntax.

`sudo nano /etc/postfix/vhosts`
```
domain.intra
```
----
This is the file that will contains all the users for the SMTP server. It's scrutured as such: `[username]@[domain] [domain]/[username]`

`sudo nano /etc/postfix/vmailbox`
```
alexander@domain.intra domain.intra/alexander/
per@domain.intra domain.intra/per/
```
----
To make postfix able to load the file correctly, we hash the file:
```
sudo postmap /etc/postfix/vmailbox
```
----
Next, create the folder that will contain all the mail. This should be owned be the mail suer that was created earlier: 
```
sudo mkdir /var/mail/vhosts
sudo chown -R vmail:vmail /var/mail/vhosts
```
----
Restart postfix for the changes to take effect:
```
sudo service postfix restart
```
## Test Postfix
In this test, we will use mail to send a mail to ourself, then check if postfix got the mail and placed it in the correct folder:
```
echo "Ploef" | mail -s Test_Message alexander@domain.intra
sudo ls /var/mail/vhosts/domain.intra/alexander/new
sudo cat /var/mail/vhosts/domain.intra/alexander/new/[mail file]
```
### Excpeted Output:
```
alexander@localhost:~$ sudo cat /var/mail/vhosts/domain.intra/alexander/new/1586290299.V802I61d66M345592.localhost.local
domain
Return-Path: <alexander@localhost.localdomain>
X-Original-To: alexander@domain.intra
Delivered-To: alexander@domain.intra
Received: by localhost.localdomain (Postfix, from userid 1000)
        id 4EF3561D64; Tue,  7 Apr 2020 20:11:39 +0000 (UTC)
Subject: Test_Message
To: <alexander@domain.intra>
X-Mailer: mail (GNU Mailutils 3.4)
Message-Id: <20200407201139.4EF3561D64@localhost.localdomain>
Date: Tue,  7 Apr 2020 20:11:39 +0000 (UTC)
From: alexander <alexander@localhost.localdomain>

Ploef
```
## Setup Dovecot
Dovecot is the IMAP service. We chould also installed the POP3 service, but IMAP is enough.
```
sudo apt install dovecot-core dovecot-imapd
```
Dovecot have a lot of configuration files, located at: `/etc/dovecot/conf.d/`. We will need to enter the files one at the time to configure docevot.

----
First, tell where dovecot will find the mails:
`sudo nano /etc/dovecot/conf.d/10-mail.conf`
```
mail_location = maildir:/var/mail/vhosts/%d/%n
```
----
Then tell dovecot how users should be able to login to the IMAP server: For testing, we will allow plaintext password, however, this will be deactivated later, when we have configured the encrypted connection.

`sudo nano /etc/dovecot/conf.d/10-auth.conf`
```
disable_plaintext_auth = no
auth_mechanisms = plain login cram-md5
auth_verbose = yes
!include auth-passwdfile.conf.ext
```
----
Then, dovecot needs to know where it can find its users. We will create two files: `/etc/dovecot/users` for the usernames and `/etc/dovecot/passwd` for the passwords:

`sudo nano /etc/dovecot/conf.d/auth-passwdfile.conf.ext`
```
passdb {
  driver = passwd-file
  args = scheme=CRYPT username_format=%u /etc/dovecot/passwd
}
```
----
Next, we will connect postfix and dovecot, allowing postfix to ask dovecot if the person logging in on the SMTP server is authorized.
We laos tells dovecot to use the user "vmail" to access the mails on the server:

`sudo nano /etc/dovecot/conf.d/10-master.conf`
```
service imap-login {
    user = vmail
    inet_listener imap {
        #port = 143
    }
    inet_listener imaps {
        #port = 933
        #ssl = yes
    }
}

service auth {
    unix_listener /var/spool/postfix/private/auth {
      mode = 0660
      user = postfix
      group = postfix
    }
}
```
----
Next, create the users file. Its syntax is: `[username]@[domain]::5000:5000::/var/mail/vhosts/[domain]/[username]/:/bin/false::`

`sudo nano /etc/dovecot/users`
```
alexander@domain.intra::5000:5000::/var/mail/vhosts/domain.intra/alexander/:/bin/false::
per@domain.intra::5000:5000::/var/mail/vhosts/domain.intra/per/:/bin/false::
```
----
Then we will create a encrypted version the passwords for the users. In this guide, we will use the password "1234" for both users:
```
doveadm pw
//Password is 1234
{CRAM-MD5}fab7bd1b4b04fae0dcaacc5d57aa331864db11efe527e9f96ed6a0ce3e134302
```
Then save the passwords for the users in the passwd file:

`sudo nano /etc/dovecot/passwd`
```
alexander@domain.intra:{CRAM-MD5}fab7bd1b4b04fae0dcaacc5d57aa331864db11efe527e9f96ed6a0ce3e134302
per@domain.intra:{CRAM-MD5}fab7bd1b4b04fae0dcaacc5d57aa331864db11efe527e9f96ed6a0ce3e134302
```
----
Restart the dovecot service for the changes to take effect:
```
sudo service dovecot restart
```
## Configure Postfix for SASL
Dovecot will listen for postfix on an internal socket, now we will tell postfix it can talk to dovecot from the socket:
```
sudo postconf -e 'smtpd_sasl_auth_enable = yes'
sudo postconf -e 'smtpd_sasl_security_options = noanonymous'
sudo postconf -e 'smtpd_client_restrictions = permit_mynetworks, permit_sasl_authenticated, reject_unauth_pipelining, reject_unknown_client_hostname'
sudo postconf -e 'smtpd_recipient_restrictions = permit_sasl_authenticated,permit_mynetworks,reject_unauth_destination'
sudo postconf -e 'smtpd_sender_restrictions = permit_mynetworks, permit_sasl_authenticated, reject_unknown_sender_domain, reject_unknown_reverse_client_hostname, reject_unknown_client_hostname'
sudo postconf -e 'smtpd_sasl_type = dovecot'
sudo postconf -e 'smtpd_sasl_path = private/auth'
```
----
Then we will tell postfix that the passwords can be MD5 encrpyed:

`sudo nano /etc/postfix/sasl/smtpd.conf`
```
mech_list: cram-md5
```
----
Restart postfix:
```
sudo service postfix restart
```
## Test Dovecot
To test dovecot, we will connect to the IMAP server via telnet:
```
telnet localhost 143
a login alexander@domain.intra 1234
b select inbox
e logout
```
### Expected Output:
```
alexander@localhost:~$ telnet localhost 143
Trying ::1...
Connected to localhost.localdomain.
Escape character is '^]'.
* OK [CAPABILITY IMAP4rev1 LITERAL+ SASL-IR LOGIN-REFERRALS ID ENABLE IDLE STARTTLS AUTH=PLAIN AUTH=LOGIN AUTH=CRAM-MD5] Dovecot (Ubuntu) ready.
a login alexander@domain.intra 1234
a OK [CAPABILITY IMAP4rev1 LITERAL+ SASL-IR LOGIN-REFERRALS ID ENABLE IDLE SORT SORT=DISPLAY THREAD=REFERENCES THREAD=REFS THREAD=ORDEREDSUBJECT MULTIAPPEND URL-PARTIAL CATENATE UNSELECT CHILDREN NAMESPACE UIDPLUS LIST-EXTENDED I18NLEVEL=1 CONDSTORE QRESYNC ESEARCH ESORT SEARCHRES WITHIN CONTEXT=SEARCH LIST-STATUS BINARY MOVE] Logged in
b select inbox
* FLAGS (\Answered \Flagged \Deleted \Seen \Draft)
* OK [PERMANENTFLAGS (\Answered \Flagged \Deleted \Seen \Draft \*)] Flags permitted.
* 1 EXISTS
* 1 RECENT
* OK [UNSEEN 1] First unseen.
* OK [UIDVALIDITY 1586291711] UIDs valid
* OK [UIDNEXT 2] Predicted next UID
b OK [READ-WRITE] Select completed (0.020 + 0.000 + 0.019 secs).
e logout
* BYE Logging out
e OK Logout completed (0.001 + 0.000 secs).
Connection closed by foreign host.
```
## Test Remote
Add the users to your mail client(thunderbird). When adding them, don't the mail client to auto configure the mail address.

The setup is:
* Email: alexander@domain.intra
* Password: 1234
* Incomming:
    * Protocol: IMAP
    * Server: 192.168.216.132
    * Port: 143
    * SSL: None
    * Authentication: Normal Password
    * Username: alexander@domain.intra
* Outgoing:
    * Protocol: SMTP
    * Server: 192.168.216.132
    * Port: 25
    * SSL: None
    * Authentication: Normal Password
    * Username: alexander@domain.intra

## TLS/SSL
For TLS/SSL, you should really use LetsEncrypt or another CA to sign your certificate 
but without a propper domain and static public IP, they can't bind your domain to your IP, 
thus not sign your certificate.

For this reason will we use a selfsigned certificate. This is less secure and browsers/mail clients will complain, 
but it allows encryption of the traffic. Plus you will easily be able to change the cert and key files for a real certificate later.
```
openssl genrsa -des3 -out server.key 2048
openssl rsa -in server.key -out domain.key
openssl req -new -sha256 -key domain.key -out domain.csr
openssl rand -writerand .rnd // Sometimes required.
openssl req -x509 -sha256 -days 365 -key domain.key -in domain.csr -out domain.cert
```
----
Then we will test if the certificate was successfully signed:
```
openssl x509 -in domain.cert -text -noout
```
### Expected Output:
```
alexander@localhost:~$ openssl x509 -in domain.cert -text -noout
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            [SECRET]
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = DK, ST = Region Syddanmark, L = Odense, O = My Company, OU = IT, CN = Alexander With, emailAddress = alexander@domain.intra
        Validity
            Not Before: Apr  7 20:53:57 2020 GMT
            Not After : Apr  7 20:53:57 2021 GMT
        Subject: C = DK, ST = Region Syddanmark, L = Odense, O = My Company, OU = IT, CN = Alexander With, emailAddress = alexander@domain.intra
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    [SECRET]
                Exponent: [SECRET]
        X509v3 extensions:
            X509v3 Subject Key Identifier:
                [SECRET]
            X509v3 Authority Key Identifier:
                keyid:[SECRET]

            X509v3 Basic Constraints: critical
                CA:TRUE
    Signature Algorithm: sha256WithRSAEncryption
         [SECRET]
```
----
Copy the created certificate and key into the ssl folder. This is to make it easier for the services to access the files, plus the Home Directory is a terrible place for server files.
```
sudo cp domain.cert /etc/ssl/certs/domain.cert
sudo cp domain.key /etc/ssl/private/domain.key
```
----
Then configure postfix to encrypt the connection.
```
sudo postconf -e 'smtpd_tls_cert_file = /etc/ssl/certs/domain.cert'
sudo postconf -e 'smtpd_tls_key_file = /etc/ssl/private/domain.key'
sudo postconf -e 'smtpd_use_tls = yes'
sudo postconf -e 'smtpd_sasl_security_options = noplaintext,noanonymous'
sudo postconf -e 'smtpd_tls_security_level = encrypt'
sudo postconf -e 'smtpd_tls_loglevel = 1'
sudo postconf -e 'smtpd_tls_session_cache_database = btree:${data_directory}/smtpd_scache'
```
In here we will enable the encryption port 465 and 587:

`sudo nano /etc/postfix/master.cf`
```
submission     inet     n    -    y    -    -    smtpd
  -o syslog_name=postfix/submission
  -o smtpd_tls_security_level=may
  -o smtpd_sasl_auth_enable=yes
  -o smtpd_relay_restrictions=permit_sasl_authenticated,reject
  -o smtpd_recipient_restrictions=permit_mynetworks,permit_sasl_authenticated,reject
  -o smtpd_sasl_type=dovecot
  -o smtpd_sasl_path=private/auth
  
smtps     inet  n       -       y       -       -       smtpd                                                          
  -o syslog_name=postfix/smtps
  -o smtpd_tls_wrappermode=yes
  -o smtpd_tls_auth_only=yes
  -o smtpd_sasl_auth_enable=yes
  -o smtpd_tls_security_level=may
  -o smtpd_relay_restrictions=permit_sasl_authenticated,reject
  -o smtpd_recipient_restrictions=permit_mynetworks,permit_sasl_authenticated,reject
  -o smtpd_sasl_type=dovecot
  -o smtpd_sasl_path=private/auth
```
----
Then, we tell dovecot that it also should encrypt the connection.

`sudo nano /etc/dovecot/conf.d/10-ssl.conf`
```
ssl = required
ssl_cert_file = </etc/ssl/certs/domain.cert
ssl_key_file = </etc/ssl/private/domain.key
ssl_prefer_server_ciphers = yes
```
Since we now have a secure way onto the server, we can disable plaintext login:

`sudo nano /etc/dovecot/conf.d/10-auth.conf`
```
disable_plaintext_auth = yes
```
----
Restart postfix and dovecot for the changes to take effect:
```
sudo service postfix restart
sudo service dovecot restart
```
## Test Certificate
Now that the certificates are in place, we will need to test if dovecot and postfix uses them, when we creates a secure connection. For the best result, do this from a remote computer:
```
openssl s_client -showcerts -connect 192.168.0.28:993
// e logout
```
### Expected Output:
```
alexander@remote:~$ openssl s_client -showcerts -connect 192.168.0.28:993
CONNECTED(00000003)
depth=0 C = DK, ST = Region Syddanmark, L = Odense, O = My Company, OU = IT, CN = Alexander With, emailAddress = alexander@domain.intra
verify error:num=18:self signed certificate
verify return:1
depth=0 C = DK, ST = Region Syddanmark, L = Odense, O = My Company, OU = IT, CN = Alexander With, emailAddress = alexander@domain.intra
verify return:1
---
Certificate chain
 0 s:/C=DK/ST=Region Syddanmark/L=Odense/O=My Company/OU=IT/CN=Alexander With/emailAddress=alexander@domain.intra
   i:/C=DK/ST=Region Syddanmark/L=Odense/O=My Company/OU=IT/CN=Alexander With/emailAddress=alexander@domain.intra
-----BEGIN CERTIFICATE-----
[SECRET]
-----END CERTIFICATE-----
---
Server certificate
subject=/C=DK/ST=Region Syddanmark/L=Odense/O=My Company/OU=IT/CN=Alexander With/emailAddress=alexander@domain.intra
issuer=/C=DK/ST=Region Syddanmark/L=Odense/O=My Company/OU=IT/CN=Alexander With/emailAddress=alexander@domain.intra
---
No client certificate CA names sent
Peer signing digest: SHA256
Server Temp Key: ECDH, P-384, 384 bits
---
SSL handshake has read 1730 bytes and written 463 bytes
---
New, TLSv1/SSLv3, Cipher is [SECRET]
Server public key is 2048 bit
Secure Renegotiation IS supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
SSL-Session:
    Protocol  : TLSv1.2
    Cipher    : [SECRET]
    Session-ID: [SECRET]
    Session-ID-ctx:
    Master-Key: [SECRET]
    Key-Arg   : None
    PSK identity: None
    PSK identity hint: None
    SRP username: None
    TLS session ticket lifetime hint: 7200 (seconds)
    TLS session ticket:
    [SECRET]

    Start Time: 1586293308
    Timeout   : 300 (sec)
    Verify return code: 18 (self signed certificate)
---
* OK [CAPABILITY IMAP4rev1 LITERAL+ SASL-IR LOGIN-REFERRALS ID ENABLE IDLE AUTH=PLAIN AUTH=LOGIN AUTH=CRAM-MD5] Dovecot (Ubuntu) ready.
e logout
* BYE Logging out
e OK Logout completed.
closed
```
```
openssl s_client -showcerts -connect 192.168.0.28:587 -starttls smtp
// quit
```
### Expected Output:
```
alexander@remote:~$ openssl s_client -showcerts -connect 192.168.0.28:587 -starttls smtp
CONNECTED(00000003)
depth=0 C = DK, ST = Region Syddanmark, L = Odense, O = My Company, OU = IT, CN = Alexander With, emailAddress = alexander@domain.intra
verify error:num=18:self signed certificate
verify return:1
depth=0 C = DK, ST = Region Syddanmark, L = Odense, O = My Company, OU = IT, CN = Alexander With, emailAddress = alexander@domain.intra
verify return:1
---
Certificate chain
 0 s:/C=DK/ST=Region Syddanmark/L=Odense/O=My Company/OU=IT/CN=Alexander With/emailAddress=alexander@domain.intra
   i:/C=DK/ST=Region Syddanmark/L=Odense/O=My Company/OU=IT/CN=Alexander With/emailAddress=alexander@domain.intra
-----BEGIN CERTIFICATE-----
[SECRET]
-----END CERTIFICATE-----
---
Server certificate
subject=/C=DK/ST=Region Syddanmark/L=Odense/O=My Company/OU=IT/CN=Alexander With/emailAddress=alexander@domain.intra
issuer=/C=DK/ST=Region Syddanmark/L=Odense/O=My Company/OU=IT/CN=Alexander With/emailAddress=alexander@domain.intra
---
No client certificate CA names sent
Peer signing digest: SHA512
Server Temp Key: ECDH, P-256, 256 bits
---
SSL handshake has read 1955 bytes and written 466 bytes
---
New, TLSv1/SSLv3, Cipher is [SECRET]
Server public key is 2048 bit
Secure Renegotiation IS supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
SSL-Session:
    Protocol  : TLSv1.2
    Cipher    : [SECRET]
    Session-ID: [SECRET]
    Session-ID-ctx:
    Master-Key: [SECRET]
    Key-Arg   : None
    PSK identity: None
    PSK identity hint: None
    SRP username: None
    TLS session ticket lifetime hint: 7200 (seconds)
    TLS session ticket:
    [SECRET]

    Start Time: 1586293497
    Timeout   : 300 (sec)
    Verify return code: 18 (self signed certificate)
---
250 SMTPUTF8
quit
221 2.0.0 Bye
closed
```
## Test Remote
Add the users to your mail client(thunderbird). When adding them, don't the mail client to auto configure the mail address. 
If they are already added, remove them and add them again.

The setup is:
* Email: alexander@domain.intra
* Password: 1234
* Incomming:
    * Protocol: IMAP
    * Server: 192.168.216.132
    * Port: 143
    * SSL: STARTTLS
    * Authentication: Encrypted Password
    * Username: alexander@domain.intra
* Outgoing:
    * Protocol: SMTP
    * Server: 192.168.216.132
    * Port: 587
    * SSL: STARTTLS
    * Authentication: Encrypted Password
    * Username: alexander@domain.intra

## TODO
* SQLite database for users, so they aren't split into two files (postfix/dovecot)

## Future Configuration
* TSL/SSL Encryption instead of STARTTLS
* SPF
* PTR
* DKIM
* DMARC
* Block Spam Mails


## Links
* https://help.ubuntu.com/lts/serverguide/postfix.html
* https://help.ubuntu.com/community/PostfixVirtualMailBoxClamSmtpHowto
* https://help.ubuntu.com/lts/serverguide/certificates-and-security.html#creating-a-self-signed-certificate
* https://help.ubuntu.com/lts/serverguide/dovecot-server.html
* https://www.namecheap.com/support/knowledgebase/article.aspx/9795/69/installing-and-configuring-ssl-on-postfixdovecot-mail-server
* https://www.linuxbabe.com/mail-server/setup-basic-postfix-mail-sever-ubuntu
* https://ubuverse.com/setting-up-your-own-mail-server-on-ubuntu/
